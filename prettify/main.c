//
// Created by 12009 on 2021/5/5.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int PageLength = 56;
int PageWedth = 60;
int LeftMargin = 10;
int HeadingLength = 5;
int FootingLength = 5;
int startPageNumber = 1;
int page = 1;
int i = 0;
char a[5];
char *aIndex = a;

void linageFull(FILE *fp2, int *linage, int *columns, int *pInt);


void printOut() {

    printf("\n");
    printf("			格式化文本文件结束\n");
    printf("版面的参数如下：\n");
    printf("页长：%d\n", PageLength);
    printf("页宽：%d\n", PageWedth);
    printf("左空白：%d\n", LeftMargin);
    printf("头长：%d\n", HeadingLength);
    printf("脚长：%d\n", FootingLength);
    printf("起始页号：%d\n", startPageNumber);
    printf("\n");
    printf("		特别说明：本程序只支持英文格式化，对于中文格式化存在乱码问题。\n");
    printf("\n");
}


void writeOut(FILE *fp2, char c1) {
    char ch1;
    ch1 = fputc(c1, fp2);
    printf("%c", ch1);
}


int zuokongbai(FILE *fp2) {
    for (i = 0; i < 10; i++)
        writeOut(fp2, ' ');
    return i;
}


void pageNumber(FILE *fp2, char *index, int *pInt) {
    int m, n;
    m = *pInt;
    int j = 0;
    while (m != 0) {
        n = m % 10;
        *(index + j) = n + '0';
        j++;
        m = m / 10;
    }

    for (j = j - 1; j >= 0; j--) {
        writeOut(fp2, *(index + j));
    }
    (*pInt)++;
}


void out(int *columns, char *array, int *linage, FILE *fp2, int *pInt, int *size) {
    if (((*(columns)) + 1 + strlen(array)) > 60) {

        if ((*(linage)) > 50) {
            linageFull(fp2, linage, columns, pInt);
            for (i = 0; i < *(size); i++) {
                writeOut(fp2, *(array + i));
                (*(columns))++;
            }
            (*(size)) = 0;
            (*(array)) = '\0';
        } else {
            writeOut(fp2, '\n');
            (*(linage))++;

            (*(columns)) = zuokongbai(fp2);

            for (i = 0; i < (*(size)); i++) {
                writeOut(fp2, (*(array + i)));
                (*(columns))++;
            }
            (*(size)) = 0;
            (*(array)) = '\0';
        }
    } else {
        writeOut(fp2, ' ');
        (*(columns))++;
        for (i = 0; i < *size; i++) {
            writeOut(fp2, *(array + i));
            (*(columns))++;
        }
        (*(size)) = 0;
        (*(array)) = '\0';
    }
}


void linageFull(FILE *fp2, int *linage, int *columns, int *pInt) {

    writeOut(fp2, '\n');
    for (i = 0; i < 29; i++) {
        writeOut(fp2, ' ');
    }
    pageNumber(fp2, aIndex, pInt);
    for (i = 0; i < 3; i++) {
        writeOut(fp2, '\n');
    }
    *linage = 0;

    for (i = 0; i < 5; i++) {
        writeOut(fp2, '\n');
        *linage++;
    }

    *columns = zuokongbai(fp2);
}


void readIn(FILE *fp1, FILE *fp2) {

    int linage = 0;
    int columns = 0;

    char array[61] = {"\0"};
    int size = 0;

    for (i = 0; i < 5; i++) {
        writeOut(fp2, '\n');
        linage++;
    }

    columns = zuokongbai(fp2);

    for (i = 0; i < 7; i++) {
        writeOut(fp2, ' ');
        columns++;
    }


    char c1;
    c1 = fgetc(fp1);
    while (c1 != EOF) {
        if (c1 == ' ') {
            if (strlen(array) == 0);
            else {

                out(&columns, array, &linage, fp2, &page, &size);
            }
        } else if (c1 == '@') {

            if (strlen(array) == 0);
            else {

                out(&columns, array, &linage, fp2, &page, &size);
            }


            if (linage > 50) {

                linageFull(fp2, &linage, &columns, &page);
                for (i = 0; i < 7; i++) {
                    writeOut(fp2, ' ');
                    columns++;
                }
            } else {
                writeOut(fp2, '\n');
                linage++;
                columns = 0;
                columns = zuokongbai(fp2);

                for (i = 0; i < 7; i++) {
                    writeOut(fp2, ' ');
                    columns++;
                }
            }
        } else {
            array[size] = c1;
            size++;
            array[size] = '\0';
        }
        c1 = fgetc(fp1);
        if (c1 == EOF) {

            out(&columns, array, &linage, fp2, &page, &size);
            break;
        }
        if (strlen(array) > 61) {
            printf("Word too long.\n");
            break;
        }
    }
}


int main(int argc, char *argv[]) {
    FILE *fp1, *fp2;
    int inputFlag = 0;
    int outputFlag = 0;
    char inputFile[100];
    char outputFile[100];

    for (i = 0; i < argc; i++) {
        if (strcmp(argv[i],"-h") == 0){
            printf("Command Line Interface Help\n");
            printf("-h    Show this help message.\n");
            printf("-o    Specify output file.\n");
            printf("-i    Specify input file.\n");
            exit(0);
        }
        if (strcmp(argv[i],"-o") == 0){
            if (i!=argc-1){
                strcpy(outputFile,argv[i+1]);
                outputFlag = 1;
            }
        }
        if (strcmp(argv[i],"-i") == 0){
            if (i!=argc-1){
                strcpy(inputFile,argv[i+1]);
                inputFlag = 1;
            }
        }
    }

    if (!inputFlag){
        printf("Plz specify input filename:\n");
        gets(inputFile);
    }

    if (!outputFlag){
        printf("Plz specify output filename:\n");
        gets(outputFile);
    }
    if ((fp1 = fopen(inputFile, "r")) == NULL) {
        printf("The input file \"%s\" was not open!\n", inputFile);
        exit(0);
    }

    if ((fp2 = fopen(outputFile, "a")) == NULL) {
        printf("The output file \"%s\" was not open!\n", outputFile);
        exit(0);
    }


    readIn(fp1, fp2);

    printOut();

    fclose(fp1);
    fclose(fp2);
}
