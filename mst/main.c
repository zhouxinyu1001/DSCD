#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define maxn 9999

typedef struct arc {
    int index;
    float weight;
    struct arc *next;
} AR;

typedef struct MyGraph {
    int type;
    int arcnum;
    int vexnum;
    char **vexname;
    AR *N;
    float **A;
} GH;

void DFS(GH *G, int *visit, int index) {
    AR *p;
    visit[index] = 1;
    p = G->N[index].next;
    while (p) {
        if (visit[p->index] == 0)
            DFS(G, visit, p->index);
        p = p->next;
    }
}

int isconnect(GH *G) {
    int i;
    int ans;
    ans = 0;
    int *visit = (int *) malloc(sizeof(int) * G->vexnum);
    memset(visit, 0, sizeof(int) * G->vexnum);
    for (i = 1; i < G->vexnum; i++) {
        if (!visit[i]) {
            DFS(G, visit, i);
            ans++;
        }
    }
    free(visit);
    if (ans == 1)
        return 1;
    else
        return 0;
}

int findvex(GH *G, char *s) {
    int i;
    for (i = 0; i < G->vexnum; i++) {
        if (strcmp(s, G->vexname[i]) == 0)
            return i;
    }
    printf("Error!\n");
    exit(EXIT_FAILURE);
}

void creatgraph(GH *G) {
    FILE *fp;
    int i, j, mg, n, k;
    char s1[20], s2[20];
    AR *p;
    fp = fopen("input.txt", "r");
    if (!fp) {
        printf("Can not open file!\n");
        exit(EXIT_FAILURE);
    }
    fscanf(fp, "%d", &n);
    G->vexnum = n;
    printf("图为有向图--1还是无向图--0：");
    scanf("%d", &k);
    G->type = k;
    G->N = (AR *) malloc(n * sizeof(AR));
    G->A = (float **) malloc(n * sizeof(int *));
    G->vexname = (char **) malloc(n * sizeof(char *));
    G->arcnum = 0;
    for (i = 0; i < n; i++) {
        fscanf(fp, "%s", s1);
        G->vexname[i] = (char *) malloc(strlen(s1) * sizeof(char));
        strcpy(G->vexname[i], s1);
        G->N[i].next = NULL;
        G->A[i] = (float *) malloc(n * sizeof(int));
        for (j = 0; j < n; j++)
            G->A[i][j] = 0;
    }
    while (fscanf(fp, "%s%s%d", s1, s2, &mg) != EOF) {
        i = findvex(G, s1);
        j = findvex(G, s2);
        (G->arcnum)++;
        p = (AR *) malloc(sizeof(AR));
        p->index = j;
        p->weight = mg;
        p->next = G->N[i].next;
        G->N[i].next = p;
        G->A[i][j] = mg;
        if (G->type == 0) {
            p = (AR *) malloc(sizeof(AR));
            p->index = i;
            G->A[j][i] = mg;
            p->weight = mg;
            p->next = G->N[j].next;
            G->N[j].next = p;
        }
    }
    fclose(fp);
}

void showgraph(GH *G) {
    int i;
    float sum = 0.;
    AR *p;
    for (i = 0; i < G->vexnum; i++) {
        printf("\n%s", G->vexname[i]);
        p = G->N[i].next;
        while (p) {
            sum = sum + p->weight;
            printf("--%s(%.2f) ", G->vexname[p->index], p->weight);
            p = p->next;
        }
    }
    printf("\n图的权值为：%f\n\n", sum);
}

int findmin(const float *a, int n) {
    int min, i, j;
    min = maxn;
    j = 0;
    for (i = 0; i < n; i++) {
        if ((a[i] != 0) && a[i] < min) {
            min = a[i];
            j = i;
        }
    }
    return j;
}

void prim(GH *G) {
    GH *prim;
    AR *a;
    int i, mmin, nex[G->vexnum];
    float min[G->vexnum];

    prim = (GH *) malloc(sizeof(GH));
    prim->vexnum = G->vexnum;
    prim->arcnum = prim->vexnum - 1;
    prim->type = 0;
    prim->N = (AR *) malloc(prim->vexnum * sizeof(AR));
    prim->vexname = (char **) malloc(prim->vexnum * sizeof(char *));
    memset(nex, 0, prim->vexnum * sizeof(int));
    for (i = 0; i < prim->vexnum; i++) {
        prim->vexname[i] = (char *) malloc(strlen(G->vexname[i]) * sizeof(char));
        strcpy(prim->vexname[i], G->vexname[i]);
        prim->N[i].next = NULL;
    }

    min[0] = 0;
    for (i = 1; i < prim->vexnum; i++)
        min[i] = maxn;
    for (a = G->N[0].next; a; a = a->next)
        min[a->index] = a->weight;
    for (i = 1; i < prim->vexnum; i++) {
        a = (AR *) malloc(sizeof(AR));
        mmin = findmin(min, prim->vexnum);
        a->index = mmin;
        a->weight = min[mmin];
        a->next = prim->N[nex[mmin]].next;
        prim->N[nex[mmin]].next = a;
        a = (AR *) malloc(sizeof(AR));
        a->index = nex[mmin];
        a->weight = min[mmin];
        a->next = prim->N[mmin].next;
        prim->N[mmin].next = a;
        min[mmin] = 0;
        for (a = G->N[mmin].next; a; a = a->next) {
            if (min[a->index] > a->weight) {
                nex[a->index] = mmin;
                min[a->index] = a->weight;
            }
        }
    }
    showgraph(prim);
}

void findmmin(GH *G, float *mmin) {
    int i, j;
    float min = 9999;
    for (i = 0; i < G->vexnum; i++) {
        for (j = 0; j < G->vexnum; j++) {
            if (G->A[i][j] != 0 && G->A[i][j] < min) {
                min = G->A[i][j];
                mmin[0] = i;
                mmin[1] = j;
                mmin[2] = min;
            }
        }
    }
    G->A[(int) (mmin[0])][(int) (mmin[1])] = 0.;
}

int get(const int *pre, int k) {
    int a;
    a = k;
    while (pre[a] != a)
        a = pre[a];
    return a;
}

void show(GH *G) {
    int i, j;
    float sum = 0.;
    for (i = 0; i < G->vexnum; i++) {
        printf("\n%s", G->vexname[i]);
        for (j = 0; j < G->vexnum; j++) {
            if (G->A[i][j]) {
                printf("--%s(%.2f)", G->vexname[j], G->A[i][j]);
                sum = sum + G->A[i][j];
            }
        }
    }
    printf("\n图的权值为：%f\n\n", sum);
}

void kruscal(GH *G) {
    GH *kruscal;
    int i, j, pre[G->vexnum];
    float mmins[3];

    kruscal = (GH *) malloc(sizeof(GH));
    kruscal->vexnum = G->vexnum;
    kruscal->arcnum = kruscal->vexnum - 1;
    kruscal->type = 0;
    kruscal->A = (float **) malloc(kruscal->vexnum * sizeof(float *));
    kruscal->vexname = (char **) malloc(kruscal->vexnum * sizeof(char *));
    for (i = 0; i < kruscal->vexnum; i++) {
        pre[i] = i;
        kruscal->vexname[i] = (char *) malloc(strlen(G->vexname[i]) * sizeof(char));
        strcpy(kruscal->vexname[i], G->vexname[i]);
        kruscal->A[i] = (float *) malloc(kruscal->vexnum * sizeof(float));
        for (j = 0; j < kruscal->vexnum; j++)
            kruscal->A[i][j] = 0;
    }

    for (i = 1; i < kruscal->vexnum;) {
        findmmin(G, mmins);
        if (get(pre, (int) (mmins[0])) != get(pre, (int) (mmins[1]))) {
            if (get(pre, (int) (mmins[0])) < get(pre, (int) (mmins[1])))
                pre[get(pre, (int) (mmins[1]))] = get(pre, (int) (mmins[0]));
            else
                pre[get(pre, (int) (mmins[0]))] = get(pre, (int) (mmins[1]));
            kruscal->A[(int) (mmins[0])][(int) (mmins[1])] = mmins[2];
            kruscal->A[(int) (mmins[1])][(int) (mmins[0])] = mmins[2];
            i++;
        }
    }
    show(kruscal);
}

int main() {
    GH G;
    creatgraph(&G);
    showgraph(&G);
    printf("\n------------------------------原图------------------------------\n\n");
    if (isconnect(&G)) {
        printf("\n------------------------------prim算法得到的最小生成树------------------------------\n");
        prim(&G);
        printf("\n------------------------------prim显示完毕------------------------------\n");
        printf("\n------------------------------kruscal算法得到的最小生成树------------------------------\n");
        kruscal(&G);
        printf("\n------------------------------kruscal显示完毕------------------------------\n");
    } else
        printf("\n不是连通图，没有最小生成树!!!\n");
    return 0;
}